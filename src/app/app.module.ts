import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {PeopleService} from "./people/people.service";
import {PeopleListComponent} from "./people/people-list.component";
import {PeopleItemComponent} from "./people/people-item.component";
import {FriendsItemComponent} from "./people/friends-item.component";
import {FriendsListComponent} from "./people/friends-list.component";
import {FofItemComponent} from "./people/fof-item.component";

@NgModule({
  declarations: [
    AppComponent,
    PeopleListComponent,
    PeopleItemComponent,
    FriendsListComponent,
    FriendsItemComponent,
    FofItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [PeopleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
