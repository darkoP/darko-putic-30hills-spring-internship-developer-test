import {Component, OnInit} from "@angular/core";
import {PeopleService} from "./people.service";

@Component({
  selector: 'list-people',
  templateUrl: './people-list.component.html'
})
export class PeopleListComponent implements  OnInit{

  public peopleItems: String[];

  constructor(private _peopleService: PeopleService) {}
  ngOnInit(): any {
    this.peopleItems = this._peopleService.getPeopleItems();
  }
}
