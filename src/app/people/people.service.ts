import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {PEOPLE_LIST} from "./people.data";
import "rxjs/add/operator/map";
import {FRIENDS_LIST} from "./friends.data";
import {filter} from "rxjs/operators";

@Injectable()
export class PeopleService {

  public peopleItems: String[];
  public friendItems: String[];
  public _id: number;
  public friends: number[];

  private _url = './assets/data.json';

  constructor(private _http: Http) {}

  getPeopleItems() {
    return this.getPeopleItem();
  }

  getPeopleItem():any {
    this._http.get(this._url)
      .map((response : Response) => response.json())
      .subscribe(resDataPeople => {
         this.peopleItems = resDataPeople;

         for (let person of this.peopleItems) {
           PEOPLE_LIST.push(person);
         }
      }
      );
    return PEOPLE_LIST;
  }

  showFriends(friendIds) {
    return this._http.get(this._url)
      .map((response : Response) =>  response.json().filter((user) => {
              return friendIds.indexOf(user.id) > -1;
      }))
  }

  // showDetails(id: number) {
  //   this._http.get(this._url)
  //     .map((response : Response) => response.json())
  //     .subscribe(
  //       resFriendsData => {
  //         return resFriendsData.filter((user) => {
  //           return user.id = id;
  //         })[0]
  //       })
  // }



}
