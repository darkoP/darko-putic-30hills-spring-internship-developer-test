import {Component, Input} from "@angular/core";
import {PeopleService} from "./people.service";
@Component({
  selector: 'friends-item',
  templateUrl: './friends-item.component.html'
})
export class FriendsItemComponent {
  public showVar1: boolean = false;
  public firendsOfFriendsList;

  @Input('test') test: any;
  @Input('friendsOfFriendsList') friendsOfFriendsList: any;

  constructor(private _friendService : PeopleService){}

  showFriends(ids){
    this.showVar1 = !this.showVar1;
    this._friendService.showFriends(ids)
      .subscribe((friends) => {
        this.firendsOfFriendsList = friends;
      });
  }

}
