export class PeopleItem {
  constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
    public age: number,
    public gender: string,
    public friends: number[]
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.gender = gender;
    this.friends = friends;
  }
}
