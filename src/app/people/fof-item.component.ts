import {Component, Input} from "@angular/core";

@Component({
  selector: 'fof-item',
  templateUrl: './fof-item.component.html'

})
export class FofItemComponent {
  @Input('test2') test2: any;
}
