import {Component, Input, OnInit} from '@angular/core';
import {PeopleItem} from "./people-item";
import {PeopleService} from "./people.service";

@Component({
  selector: 'item-component',
  templateUrl: './people-item.component.html'
})

export class PeopleItemComponent{

  public showVar: boolean = false;
  public firendsOfFriendsList;
  public friendsList;


  @Input('item') peopleItem: PeopleItem;

  constructor(private _friendService : PeopleService){}

  // showDetails(id){
  //   this.userDetails = this._friendService.showDetails(id);
  // }

  showFriends(ids){
    this.showVar = !this.showVar;
    this._friendService.showFriends(ids)
      .subscribe((friends) => {
        this.friendsList = friends;
      });
  }

}
