import {Component, Input} from "@angular/core";

@Component({
  selector: 'friends-list',
  templateUrl: './friends-list.component.html'
})
export class FriendsListComponent {
  @Input('friendsList') friendsList: any;
}
